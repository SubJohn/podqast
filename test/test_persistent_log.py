from podcast.persistent_log import *

def test_store_and_read():
    for i in range(0,1005):
        persist_log(LogType.Exception, num=str(i))
    log = list(get_log_messages())
    assert len(log) == 1000
    assert log[0].params == '{"num": "1004"}'
    assert log[-1].params == '{"num": "5"}'
