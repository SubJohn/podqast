"""
Test the Store class
"""

import sys

from podcast.constants import Constants

sys.path.append("../python")

import os

from podcast.deprecatedstore import DeprecatedStore

store = None


def test_store_and_get():
    """
    Test the store function and do I get the data back?
    """
    directory = Constants().storepath
    len1 = len(os.listdir(directory))
    store = DeprecatedStore(directory)
    store.store("test1", 1)
    len2 = len(os.listdir(directory))
    
    element = store.get("test1")
    element2 = store.get("doesnotexist")

    assert len2 == len1 + 1
    assert element == 1
    assert not element2
    
def test_delete():
    directory = Constants().storepath
    store = DeprecatedStore(directory)
    len1 = len(os.listdir(directory))
    store.delete("test1")
    store.delete("idonotexist")           # no exception here!
    len2 = len(os.listdir(directory))

    assert len2 == len1 - 1
