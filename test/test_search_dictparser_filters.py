from podcast.podcast import Podcast
from podcast.search.DictParser import *


def test_empty_filter():
    assert create_filter_from_options({}) == None


def test_single_filters():
    dummy_pod = Podcast()
    assert create_filter_from_options({"searchtext": "abc"}) == PodpostTextFilter("abc")

    assert create_filter_from_options({"fav_filter": True}) == FavoriteFilter(True)
    assert create_filter_from_options({"fav_filter": False}) == FavoriteFilter(False)

    assert create_filter_from_options({"listened_filter": True}) == PodpostListenedFilter(True)
    assert create_filter_from_options({"listened_filter": False}) == PodpostListenedFilter(False)

    assert create_filter_from_options({"podcast_url": "abc"}, {"abc": dummy_pod}) == PodcastFilter(dummy_pod)


def test_combining_filters():
    assert create_filter_from_options({"searchtext": "abc", "fav_filter": True}) == AndFilter(PodpostTextFilter("abc"),
                                                                                              FavoriteFilter(True))
