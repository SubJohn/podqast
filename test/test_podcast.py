"""
Test Podcast functions
"""
import logging
from typing import Tuple, List

import httpretty
import pytest
from httpretty import HTTPretty

from peewee import DoesNotExist
from podcast import POST_ID_TYPE
from podcast.persistent_log import get_log_messages, LogType
from podcast.podcast import Podcast, PodcastFactory
from podcast.podcastlist import PodcastList, PodcastListFactory
from podcast.podpost import PodpostFactory, Podpost, PodpostChapter
from podcast.queue import QueueFactory, QueueData
from podcast.util import ilen, MovePostToPage
from test import xml_headers, read_testdata

logger = logging.getLogger(__name__)


@httpretty.activate(allow_net_connect=False)
def test_feed_entry():
    """
    Get a feed entry
    """
    url_sn = "http://feeds.twit.tv/sn.xml"
    HTTPretty.register_uri(HTTPretty.GET, url_sn,
                           body=read_testdata('testdata/twittv.xml'), adding_headers=xml_headers)
    url_sn = "https://feeds.twit.tv/sn.xml"
    HTTPretty.register_uri(HTTPretty.GET, url_sn,
                           body=read_testdata('testdata/twittv.xml'), adding_headers=xml_headers)
    url_fs = 'https://freakshow.fm/feed/opus/'
    HTTPretty.register_uri(HTTPretty.GET, url_fs,
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)
    image = b'\xff\xd8\xff\xe0\x00\x10JFIF\x00\x01\x01\x01\x00}\x00}\x00\x00\xff\xdb\x00C\x00\x08\x06\x06\x07\x06\x05\x08\x07\x07\x00\xf7\xfa(\xa2\x80\n(\xa2\x80\n(\xa2\x80\n(\xa2\x80\n(\xa2\x80\n(\xa2\x80\n(\xa2\x80\n(\xa2\x80\n(\xa2\x80?\xff\xd9'
    HTTPretty.register_uri(HTTPretty.GET, 'https://meta.metaebene.me/media/mm/freakshow-logo-1.0.jpg',
                           body=image)
    HTTPretty.register_uri(HTTPretty.GET,
                           'https://elroy.twit.tv/sites/default/files/styles/twit_album_art_144x144/public/images/shows/security_now/album_art/audio/sn_albumart_mask.jpg?itok=VEh3JGKV',
                           body=image)
    HTTPretty.register_uri(HTTPretty.GET,
                           'https://elroy.twit.tv/sites/default/files/styles/twit_album_art_2048x2048/public/images/shows/security_now/album_art/audio/sn_albumart_mask.jpg?itok=scC8c-TL',
                           body=image)

    seen_postids = {}

    pclist = PodcastList()
    assert_pclist_empty(pclist)

    for url, episode_count in [(url_fs, 20),
                               (url_sn, 10)]:
        podcast, episodes = Podcast.create_from_url(url)
        podcast.set_params({'move': 2, 'autolimit': 1337, 'playrate': 2.8})
        podcast = PodcastFactory().get_podcast(url)  #
        assert podcast.logo_path.startswith("/")
        assert podcast.get_params() == {'move': 2, 'subscribed': True, 'autolimit': 1337, 'playrate': 2.8}
        assert podcast.alt_feeds != None
        for feed in podcast.alt_feeds:
            assert type(feed['url']) == str
            assert type(feed['title']) == str
        feedinfo = podcast.feedinfo()
        assert type(feedinfo["url"]) == str
        assert type(feedinfo["title"]) == str
        assert type(feedinfo["description"]) == str
        assert type(feedinfo["descriptionfull"]) == str
        assert type(feedinfo["logo_url"]) == str
        assert podcast.get_latest_entry() != None
        post_objects = list(PodpostFactory().get_podcast_posts(podcast))
        assert len(post_objects) == episode_count
        for post in post_objects:
            if post.id in seen_postids.keys():
                print("duplicate postid %d oldtitle: %s newtitle: %s" % (post.id, seen_postids[post.id], post.title))
                assert False
            seen_postids[post.id] = post.title
            assert post.duration >= 0
            if url == url_fs:
                assert len(post.get_chapters()) > 0
            for i in range(len(post.get_chapters())):
                post.toggle_chapter(i)
            for chapter in post.get_chapters():
                assert chapter
                assert type(chapter) == dict
                assert 'title' in chapter
                assert 'start_millis' in chapter
                assert type(chapter['start_millis']) == int
                assert 'selected' in chapter
                assert chapter['selected'] == False
                assert type(chapter['start_millis'] == float)
        entries = list(podcast.get_entries())
        assert len(entries) == episode_count
        for data in entries:
            assert data != None
            assert type(data) == dict
            assert 'id' in data.keys()
            assert 'podcast_logo' in data.keys()
            assert 'episode_logo' in data.keys()
            assert 'description' in data.keys()
            assert type(data['id']) == POST_ID_TYPE
            assert len(data['podcast_logo']) > 0 or data['podcast_logo'] == ""
            assert type(data['podcast_logo']) == str
            assert podcast.get_entry(data['id']) != None
            assert data == podcast.get_entry(data['id']).get_data()

    assert pclist.get_podcast_count() == 2
    assert ilen(pclist.get_podcasts_objects()) == 2
    assert ilen(pclist.get_podcasts_objects(subscribed_only=True)) == 2
    assert url_sn in list(pclist.get_podcasts())
    assert url_fs in list(pclist.get_podcasts())

    for url in [url_sn, url_fs]:
        entries = PodcastFactory().get_podcast(url).entry_ids_old_to_new
        PodcastFactory().remove_podcast(url)
        for entry_id in entries:
            assert PodpostFactory().get_podpost(entry_id) is None
            assert PodpostChapter.select().where(PodpostChapter.podpost == entry_id).count() == 0
        try:
            Podcast.get(Podcast.url == url)
            assert False  # not properly removed
        except DoesNotExist:
            pass
    assert_pclist_empty(pclist)


def assert_pclist_empty(pclist):
    assert pclist.get_podcast_count() == 0
    assert ilen(pclist.get_podcasts_objects()) == 0
    assert ilen(pclist.get_podcasts_objects(subscribed_only=True)) == 0
    assert ilen(pclist.get_podcasts()) == 0


def test_import_nohref():
    url_99 = "https://feeds.99percentinvisible.org/99percentinvisible"
    HTTPretty.register_uri(HTTPretty.GET, url_99,
                           body=read_testdata('testdata/99percentinvisible.xml'), adding_headers=xml_headers)
    Podcast.create_from_url(url_99)


@httpretty.activate(allow_net_connect=False)
def test_preview():
    invoked = 0

    def request_callback(request, uri, response_headers):
        nonlocal invoked
        invoked += 1
        logger.info("Returning normal response file")
        return [200, response_headers, read_testdata('testdata/freakshow.rss')]

    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=request_callback, adding_headers=xml_headers)
    podcast, entries = Podcast.create_from_url("https://freakshow.fm/feed/opus/", preview=True)
    assert Podcast.select().count() == 0
    assert Podpost.select().count() == 0
    assert len(entries) > 0
    assert len(podcast.alt_feeds) == 4
    assert invoked == 1
    Podcast.create_from_url("https://freakshow.fm/feed/opus/", preview=False)
    assert invoked == 1


@httpretty.activate(allow_net_connect=False)
def test_feed_no_changes():
    global invoked
    invoked = 0

    def request_callback(request, uri, response_headers):
        global invoked
        invoked += 1
        if invoked == 1:
            logger.info("Returning normal response file")
            return [200, response_headers, read_testdata('testdata/freakshow.rss')]
        if invoked == 2:
            logger.info("Returning 304")
            assert request.headers.get('If-Modified-Since') != None
            return [304, response_headers, ""]
        logger.error("request callback error")
        assert False

    feedurl = 'https://freakshow.fm/feed/opus/'
    HTTPretty.register_uri(HTTPretty.GET, feedurl,
                           body=request_callback, adding_headers=xml_headers)
    podcast, episodes = Podcast.create_from_url(feedurl)
    assert podcast
    assert invoked == 1
    assert ilen(podcast.refresh(moveto=-1)) == 0
    assert invoked == 2


@pytest.mark.parametrize("refreshable_podcast_fixture", ["testdata/fakefeed"], indirect=True)
def test_feed_refresh(refreshable_podcast_fixture):
    p, episodes = refreshable_podcast_fixture
    assert len(episodes) == 1
    new_episodes = list(p.refresh(0))
    assert len(new_episodes) == 1
    assert len(p.entry_ids_old_to_new) == 2
    assert [e['title'] for e in p.get_entries()] == ['Hello, I am a new fake episode for testing!!öäü',
                                                     'Hello, I am a fake episode for testing!!öäü']


@pytest.mark.parametrize("refreshable_podcast_fixture", ["testdata/fakefeed"], indirect=True)
def test_podcastlist_refresh(refreshable_podcast_fixture):
    p, episodes = refreshable_podcast_fixture
    plist = PodcastList()
    assert ilen(p.get_entries()) == 1
    assert plist.get_podcast_count() == 1
    assert list(plist.refresh(0)) == [(2, p.title, 'Hello, I am a new fake episode for testing!!öäü', 0)]

@pytest.mark.parametrize("refreshable_podcast_fixture", ["testdata/fakefeed"], indirect=True)
def test_move(refreshable_podcast_fixture):
    p, episodes = refreshable_podcast_fixture
    plist = PodcastListFactory().get_podcast_list()
    p.move = MovePostToPage.QUEUE_TOP.value
    assert p.move == 1
    p.save()
    assert QueueFactory().get_queue().podposts == []
    assert ilen(p.get_entries()) == 1
    assert plist.get_podcast_count() == 1
    list(plist.refresh(0))
    assert len(QueueFactory().get_queue().podposts) == 1
    assert len(QueueData.get_queue()) == 1



@pytest.fixture
def refreshable_podcast_fixture(request) -> Tuple[Podcast, List[Podpost]]:
    """
    A pytest fixture to setup a feed that uses a different file when refreshed.
    @param request: the first parameter should be the filename without xml. Thus loads <filename>.xml and then <filename>2.xml
    """
    httpretty.enable()
    invoked = 0
    filename = request.param

    def request_callback(request, uri, response_headers):
        nonlocal invoked
        invoked += 1
        logger.info("Returning normal response file")
        testdata = read_testdata(filename + ".xml") if invoked == 1 else read_testdata(filename + "2.xml")
        return [200, response_headers, testdata]

    feed_url = 'http://fakefeed.com/feed'
    HTTPretty.register_uri(HTTPretty.GET, feed_url,
                           body=request_callback, adding_headers=xml_headers)
    yield Podcast.create_from_url(feed_url)
    assert invoked > 0




@httpretty.activate(allow_net_connect=False)
def test_pagination_stops():
    url_f1 = "https://fakefeed.com/"
    url_f2 = "https://fakefeed.com/page2"

    invoked = 0

    def request_callback(request, uri, response_headers):
        nonlocal invoked
        invoked += 1
        logger.info("Returning normal response file")
        testdata = read_testdata('testdata/fakefeed.xml') if invoked == 1 else read_testdata(
            'testdata/pagedfakefeed.xml')
        return [200, response_headers, testdata]

    HTTPretty.register_uri(HTTPretty.GET, url_f1, body=request_callback,
                           adding_headers=xml_headers)
    HTTPretty.register_uri(HTTPretty.GET, url_f2, body=read_testdata('testdata/fakefeed.xml'),
                           adding_headers=xml_headers)
    podcast, episodes = Podcast.create_from_url(url_f1)
    assert 1 == podcast.count_episodes()
    assert 1 == len(episodes)

    episodes = list(podcast.refresh(0))
    assert 2 == podcast.count_episodes()
    assert 1 == len(episodes)


@httpretty.activate(allow_net_connect=False)
def test_pagination():
    url_f = "https://fakefeed.com/page"
    HTTPretty.register_uri(HTTPretty.GET, url_f, body=read_testdata('testdata/fakefeed2.xml'),
                           adding_headers=xml_headers)
    podcast, episodes = Podcast.create_from_url(url_f)
    assert 2 == podcast.count_episodes()
    Podpost.delete_by_id(episodes[1].id)
    assert 1 == podcast.count_episodes()
    list(podcast.refresh(0, 0, True))
    assert 2 == podcast.count_episodes()


@httpretty.activate(allow_net_connect=False)
def test_new_feed_url():
    url_f = "https://fakefeed.com/"
    HTTPretty.register_uri(HTTPretty.GET, url_f, body=read_testdata('testdata/new-feed-url-feed.rss'),
                           adding_headers=xml_headers)
    url_f1 = "https://newhoster.com/feed/ogg"
    HTTPretty.register_uri(HTTPretty.GET, url_f1, body=read_testdata('testdata/fakefeed.xml'),
                           adding_headers=xml_headers)
    podcast, episodes = Podcast.create_from_url(url_f)
    assert podcast.url == "https://newhoster.com/feed/ogg"
    assert 1 == ilen(get_log_messages())
    assert list(get_log_messages())[0].messagetype == LogType.FeedRedirect.name


@pytest.mark.parametrize("refreshable_podcast_fixture", ["testdata/feed_entries_not_ordered"], indirect=True)
def test_podcastlist_refresh(refreshable_podcast_fixture):
    p, episodes = refreshable_podcast_fixture
    plist = PodcastList()
    assert ilen(p.get_entries()) == 1
    assert plist.get_podcast_count() == 1
    list(plist.refresh(0))
    assert ilen(p.get_entries()) == 2
