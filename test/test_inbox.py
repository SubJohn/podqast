"""
test the archive
"""

import sys

from podcast.inbox import InboxFactory
from . import setup_inbox_with_2_posts

sys.path.append("../python")

from podcast.archive import ArchiveFactory


def test_insert():
    """
    """

    inbox = InboxFactory().get_inbox()
    entry1, entry2 = setup_inbox_with_2_posts()

    result_posts = list(inbox.get_podposts())
    assert entry1.id in result_posts
    assert entry2.id in result_posts
    inbox.move_archive(entry1.id)
    assert list(inbox.get_podposts()) == [entry2.id]
    archive = ArchiveFactory().get_archive()
    assert entry1.id in list(archive.get_podposts())
    inbox.move_all_archive()
    assert list(inbox.get_podposts()) == []
    assert entry1.id in list(archive.get_podposts())
    assert entry2.id in list(archive.get_podposts())

    # test post hickup where posts somehow get into inbox and archive at the same time
    inbox.insert(entry1.id)
    inbox.insert(entry2.id)
    archive.remove_podpost(entry1.id)
    inbox.move_all_archive()
    assert entry1.id in list(archive.get_podposts())
    assert entry2.id in list(archive.get_podposts())

