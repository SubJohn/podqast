"""
Podcast
"""

import sys
from typing import List, Dict, Optional, Iterator, Tuple

from feedparser import FeedParserDict
from peewee import CharField, TextField, BooleanField, IntegerField, ModelSelect, DoesNotExist, FloatField, \
    IntegrityError
from podcast.persistent_log import persist_log, LogType
from podcast.util import movePost

sys.path.append("/usr/share/podqast/python")

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from podpost import Podpost

from podcast.constants import Constants, BaseModel

from podcast.factory import BaseFactory
from podcast.feedutils import fetch_feed, FeedFetchingError, NotModified, NewFeedUrlError

from podcast.cache import Cache
from podcast import util, feedutils
from email.utils import mktime_tz, parsedate_tz
from calendar import timegm

import hashlib
import os
import logging
import pyotherside

logger = logging.getLogger(__name__)

# only used for caching the already parsed feeds that were previewed and are now used to create a podcast
feedCache = Cache()


class Podcast(BaseModel):
    """
    A saved podcast
    """

    alt_feeds: List[Dict[str, str]]
    autolimit = IntegerField(null=True)
    logo_path: Optional[str] = CharField(null=True)
    logo_url: str = CharField(default="")
    description: str = TextField(default="")
    title: CharField = CharField(default="")
    url: CharField = CharField(unique=True, index=True)
    link: str = CharField()
    subscribed: bool = BooleanField(default=False)
    playrate = FloatField(default=1.0)
    published: int = IntegerField(default=0)
    move: int = IntegerField(default=0)
    episodes: ModelSelect

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def create_from_url(cls, url, preview=False, num_preview_items=3) -> Optional[Tuple['Podcast', List['Podpost']]]:
        """

        @rtype: Tuple[Podcast,List[Podpost]]
        """
        try:
            if PodcastFactory().get_podcast(url) is not None:
                logger.warning("Podcast %s did already exist", url)
                podcast = PodcastFactory().get_podcast(url)
            else:
                podcast = cls()
            podcast.subscribed = not preview
            logger.info("initializing podcast %s", url)
            podcast.url = url
            feed: FeedParserDict
            if feedCache.exists(url):
                feed = feedCache.get(url)
            else:
                try:
                    feed = fetch_feed(0, url)
                    if preview:
                        feedCache.store(url, feed)
                except NotModified:
                    raise FeedFetchingError("Should not get 304 on initial fetching")
                except NewFeedUrlError as e:
                    persist_log(LogType.FeedRedirect, msg="permanently redirected feed",
                                oldurl=url, newurl=e.url)
                    return cls.create_from_url(e.url, preview, num_preview_items)
            episodes = cls.podcast_from_feed(feed, podcast, preview, url, num_preview_items)
            return podcast, episodes
        except ValueError:
            logger.exception("error while parsing")
            return
        except FeedFetchingError:
            logger.exception("error while parsing")
            return

    @classmethod
    def podcast_from_feed(cls, feed: FeedParserDict, podcast: 'Podcast', preview: bool, url: str, num_preview_items: int = 3) -> List['Podpost']:
        try:
            podcast.title = feed.feed.title
        except:
            logger.exception("podcast: no title")
            raise ValueError("Podcast has no name")
        try:
            podcast.link = feed.feed.link
        except:
            podcast.link = url
        try:
            podcast.description = feed.feed.description
        except:
            podcast.description = ""
        podcast.__set_data_from_feed(feed)
        if preview:
            episodes = podcast.__process_episodes(feed, limit=num_preview_items, supress_limit_notification=True,
                                                  do_not_store=True)
        else:
            PodcastFactory().create(podcast)
            episodes = podcast.__process_episodes(feed)
        return episodes

    def __set_logo_from_feed(self, feed: FeedParserDict):
        try:
            if not feed or "image" not in feed.feed or not feed.feed.image.href:
                return #not there
            if self.logo_path and os.path.exists(self.logo_path) and feed.feed.image.href == self.logo_url:
                return #already downloaded
            self.logo_url = feed.feed.image.href
            logger.debug("podcast new logo_url: %s", self.logo_url)
            old_path = self.logo_path
            if self.logo_path:
                self.logo_path = None
            self.__download_icon()
            if old_path:
                util.delete_file(old_path)
        except:
            self.logo_url = "../../images/podcast.png"
            logger.exception("podcast logo_url error")

    def __process_episodes(self, feed, limit=0, supress_limit_notification=False, do_not_store=False,
                           break_on_first_existing_episode=True) -> List['Podpost']:
        """
        returns the episodes in order from newest to oldest
        """

        logger.debug("fetching episodes for %s full=%s limit=%d", self.title, str(not break_on_first_existing_episode),
                     limit)

        known_guids = set()
        known_hrefs = set()
        from podcast.podpost import Podpost
        for known_entry in Podpost.select(Podpost.guid, Podpost.href).where(Podpost.podcast == self):
            known_guids.add(known_entry.guid)
            known_hrefs.add(known_entry.href)

        def entry_already_known(entry):
            nonlocal known_guids, known_hrefs
            href_known = "href" in entry and entry.href in known_hrefs
            has_guid = "guid" in entry
            guid_known = has_guid and entry["guid"] in known_guids
            # logger.debug("entry knowncheck: %s %s %s", has_guid, guid_known, href_known)
            return guid_known or href_known

        if self.autolimit:
            logger.debug("Overwriting limit (%d) with autolimit (%d)", limit, self.autolimit)
            limit = self.autolimit

        episode_count = 0
        new_posts = []
        for entry in feedutils.iterate_feed_entries(feed):
            if entry_already_known(entry):
                pyotherside.send("refreshPost", None)
                if break_on_first_existing_episode:
                    break
                else:
                    continue
            try:
                post = Podpost.from_feed_entry(self, entry, self.url)
                episode_count += 1
                new_posts.append(post)
            except Exception as e:
                logger.exception("could not process entry of podcast %s", self.url)
                persist_log(LogType.FeedEpisodeParseError, title=self.title, entry=entry, exception=e)
                continue
            # todo: limit stuff probably broken
            if limit != 0 and episode_count >= limit:
                if not supress_limit_notification:
                    persist_log(LogType.AutoPostLimit, id=self.id, title=self.title, limit=limit)
                break
        if not do_not_store:
            from podcast.podpost import PodpostFactory
            PodpostFactory().persist_bulk(new_posts)
        return new_posts

    @property
    def entry_ids_old_to_new(self) -> List[int]:
        from podcast.podpost import Podpost
        return [post.id for post in self.episodes.select(Podpost.id).order_by(Podpost.published)]

    def logo(self) -> str:
        if self.logo_path:
            image = self.logo_path
        else:
            image = self.logo_url
        return image

    def __set_data_from_feed(self, feed):
        logger.debug("setting instance values from feed")
        self.__set_logo_from_feed(feed)
        self.published = self.__get_times_from_feeds(feed)
        self._set_alt_feeds(feed)

    def __get_times_from_feeds(self, feed):
        if "updated_parsed" in feed.feed:
            return timegm(feed.feed.updated_parsed)
        elif "date" in feed.headers or "Date" in feed.headers:
            header = feed.headers["Date"] if "Date" in feed.headers else feed.headers["date"]
            return mktime_tz(parsedate_tz(header))
        else:
            return 0

    def feedinfo(self) -> Dict[str, object]:
        """
        return feed information
        """

        title = self.title
        description = descriptionfull = self.description

        if len(description) > 160:
            description = description[:160] + "..."

        return {
            "url": self.url,
            "link": self.link,
            "title": title,
            "description": description,
            "descriptionfull": descriptionfull,
            "logo_url": self.logo(),
            "episodecount": self.episodes.count()
        }

    def _set_alt_feeds(self, feed):
        """
        return all alternative feeds
        """
        self.alt_feeds = []
        links = feedutils.get_feed_links(feed)
        logger.info("setting alt_feeds found %d feeds", len(links))
        for link in links:
            if "type" in link and self.__is_valid_alt_feed(link):
                if "title" in link:
                    title = link.title
                else:
                    title = ""
                self.alt_feeds.append({"url": link.href, "title": title})
                logger.debug("setting alt feed: %s %s", title, link.href)

    def __is_valid_alt_feed(self, link):
        return (link.type == "application/rss+xml" or link.type == "application/atom+xml") and (
                not "rel" in link or link.rel == 'alternate' or link.rel == 'self')

    def get_entry(self, id) -> 'Podpost':
        """
        Get an entry from podcast by id
        """

        from podcast.podpost import PodpostFactory
        return PodpostFactory().get_podpost(id)

    def get_entries(self) -> Iterator[Dict]:
        """
        Return a list of entries
        """
        logger.debug("Podcast get_entries started.")
        from podcast.podpost import PodpostFactory
        for post in PodpostFactory().get_podcast_posts(self):
            yield post.get_data()

    def get_latest_entry(self) -> Optional['Podpost']:
        """
        Get the newest entry from podcast
        returns None if there are no entries
        """
        if len(self.entry_ids_old_to_new) > 0:
            return self.get_entry(self.entry_ids_old_to_new[-1])
        else:
            return None

    def __download_icon(self):
        """
        Download icon
        """

        iconpath = Constants().iconpath
        rdot = self.logo_url.rfind(".")
        extexcl = self.logo_url.rfind("?")
        if extexcl > 0:
            ext = self.logo_url[rdot + 1: extexcl]
        else:
            ext = self.logo_url[rdot + 1:]
        if ext == "jpg" or ext == "jpeg" or ext == "png" or ext == "gif":
            name = (
                    hashlib.sha256(self.logo_url.encode()).hexdigest() + "." + ext
            )

            logo_path = os.path.join(iconpath, name)
            try:
                util.dl_from_url(self.logo_url, logo_path)
            except:
                logo_path = ""
                logger.info(
                    "could not load icon: %s", str(sys.exc_info()[0])
                )
            self.logo_path = logo_path
        else:
            self.logo_path = ""

    def refresh(self, moveto, limit=0, full_refresh=False) -> Iterator[Tuple[int, 'Podpost']]:
        """
        Refresh podcast and return list of new entries
        @type moveto: int the page the podcast should be moved to (see FeedParser#movePost)
        """
        logger.info("Refreshing %s", self.title)
        pyotherside.send("refreshPost", self.title)
        try:
            feed = fetch_feed(self.published if not full_refresh else 0, self.url)

            self.__set_data_from_feed(feed)
            PodcastFactory().persist(self)

            move = self.move
            if move < 0:
                move = moveto

            logger.debug("Moveto is %d", move)

            postCount = 0
            for post in self.__process_refreshed_feedentries(feed, limit, full_refresh):
                logger.debug("Adding post %s", post.title)
                postCount += 1
                movePost(move, post.id)
                yield (move, post)
            persist_log(LogType.SuccessfulRefresh, title=self.title, num_new_episodes=postCount)
        except NotModified:
            logger.info("Got 304 response, skipping")
            persist_log(LogType.Refresh304, title=self.title)
        except FeedFetchingError as ffe:
            logger.exception("Could not fetch feed")
            persist_log(LogType.FeedParseError, title=self.title, errormsg=ffe)
        except NewFeedUrlError as e:
            self.url = e.url
            PodcastFactory().persist(self)
            persist_log(LogType.FeedRedirect, title=self.title, msg="permanently redirected feed", oldurl=self.url,
                        newurl=e.url)
            yield from self.refresh(moveto, limit, full_refresh)
        except Exception as e:
            logger.exception("Could not fetch feed")
            persist_log(LogType.Exception, msg="during refresh", podcasttitle=self.title, exception=e)

        pyotherside.send("refreshPost", None)

    def __process_refreshed_feedentries(self, feed, limit, full_refresh) -> List['Podpost']:
        """
        saves new entries for this feed
        yields all new entries
        """
        new_posts = self.__process_episodes(feed, 0 if full_refresh else limit,
                                            break_on_first_existing_episode=not full_refresh)
        logger.info("Found %d new entries.", len(new_posts))
        return new_posts

    def set_params(self, params):
        """
        Set the Parameters
        """

        self.move = params["move"]
        self.autolimit = params["autolimit"]
        self.playrate = params["playrate"]
        PodcastFactory().persist(self)

    def get_params(self) -> Dict[str, object]:
        """
        Return a set of parameters
        """

        try:
            if self.subscribed:
                subscribed = True
            else:
                subscribed = False
        except:
            subscribed = False
        try:
            autolimit = self.autolimit
        except:
            autolimit = self.autolimit = None
        try:
            playrate = self.playrate
        except:
            playrate = self.playrate = 0

        return {
            "move": self.move,
            "subscribed": subscribed,
            "autolimit": autolimit,
            "playrate": playrate,
        }

    def count_episodes(self):
        return self.episodes.count()


class PodcastFactory(BaseFactory):
    """
    Helping Factory
    """
    podcastcache: Cache

    def __init__(self):
        super().__init__()
        self.podcastcache = Cache(50)

    def get_podcast(self, url) -> Optional[Podcast]:
        """
        Get a new podcast instance with name url
        """

        podcast = self.podcastcache.get(url)
        if not podcast:
            try:
                podcast = Podcast.get(Podcast.url == url)
                self.podcastcache.store(url, podcast)
                return podcast
            except DoesNotExist:
                logger.warning("Podcast %s does not exist in storage", url)
                return None
        else:
            return podcast

    def remove_podcast(self, url: str):
        logger.info("deleting podcast and entries %s", url)
        to_delete = self.get_podcast(url)
        if to_delete != None:
            self.podcastcache.delete(url)
            self.delete(to_delete)

    def persist(self, podcast: Podcast):
        podcast.save()
        self.podcastcache.store(podcast.url, podcast)

    def create(self, podcast: Podcast):
        """
        ersists a new podcast in the database
        """
        try:
            podcast.save(force_insert=True)
        except IntegrityError:
            logger.error("The podcast %s already existed, overwriting existing podcast", podcast.url)
            podcast.id = self.get_podcast(podcast.url).id
            podcast.save()
        self.podcastcache.store(podcast.url, podcast)

    def delete(self, podcast: Podcast):
        podcast.delete_instance()
        self.podcastcache.delete(podcast.url)
