import os

import pyotherside
from typing import Optional

import logging
import threading

from peewee import Model
from playhouse.sqliteq import SqliteQueueDatabase
from podcast import singleton, util

logger = logging.getLogger(__name__)

dbinitlock: threading.Lock = threading.Lock()
# sqlite does not easily allow concurrent writes, sqlite queue db allows us to write without retry mechanisms when other writed block the db
db: SqliteQueueDatabase = SqliteQueueDatabase(None, autostart=False, autoconnect=False)


class Constants(metaclass=singleton.Singleton):
    """
    manages constants supplied by qml like süecific paths etc
    """
    markListenedBeforeEndThreshold: int
    audiofilepath: str  # audiofilepath
    iconpath: str
    storepath: str
    backuppath: str
    cache_home: str
    config_home: str
    data_home: str
    favorites_home: Optional[str]
    media_home: Optional[str]
    external_home: Optional[str]

    object: Optional[object]

    def __init__(self, progname="harbour-podqast"):
        self.progname = progname
        self.__initialize(progname)

    def __initialize(self, progname):
        with dbinitlock:
            global db
            self.object = None
            self.media_home = None
            self.favorites_home = None
            self.external_home = None
            self.markListenedBeforeEndThreshold = 60

            home = os.path.expanduser("~")
            xdg_data_home = os.environ.get(
                "XDG_DATA_HOME", os.path.join(home, ".local", "share")
            )
            xdg_config_home = os.environ.get(
                "XDG_CONFIG_HOME", os.path.join(home, ".config")
            )
            xdg_cache_home = os.environ.get(
                "XDG_CACHE_HOME", os.path.join(home, ".cache")
            )

            if "PODQAST_HOME" in os.environ:
                self.data_home = self.config_home = self.cache_home = os.environ["PODQAST_HOME"]
            else:
                self.data_home = os.path.join(xdg_data_home, progname)
                self.config_home = os.path.join(xdg_config_home, progname)
                self.cache_home = os.path.join(xdg_cache_home, progname)
            self.storepath = os.path.join(self.data_home, "store")
            self.iconpath = os.path.join(self.data_home, "icons")
            self.audiofilepath = os.path.join(self.data_home, "audio")
            self.backuppath = os.path.join(home, "Downloads")
            logger.info("Base storage path: %s", self.data_home)
            util.make_directory(self.data_home)
            util.make_directory(self.config_home)
            util.make_directory(self.cache_home)
            util.make_directory(self.storepath)
            util.make_directory(self.iconpath)
            util.make_directory(self.audiofilepath)
            self.sqlitepath = os.path.join(self.data_home, "podqast.sqlite")
            db.autoconnect = True
            db.init(self.sqlitepath, timeout=1000, pragmas={
                'journal_mode': 'wal',
                'cache_size': -1 * 64000,  # 64MB
                'foreign_keys': 1,
                'ignore_check_constraints': 0,
                'synchronous': 0})
            db.start()
            db.connect()
            logger.info("connected to sqlite file: %s, open: %s, id: %s/%s", db.database, db.is_connection_usable(),
                        id(self), id(db))

    def init_from_qml(self, object):
        """
        Get QML object
        """
        if self.object:
            return
        self.object = object
        home = os.path.expanduser("~")
        self.media_home = os.path.join(self.get_val("musicHomeVal"), "podqast")
        self.favorites_home = os.path.join(self.media_home, "favorites")
        self.external_home = os.path.join(self.media_home, "external")

        xdg_media_home = os.path.join(
            "XDG_DATA_DIRS", os.path.join(home, "podqast")
        )
        if os.path.exists(xdg_media_home):
            os.rename(xdg_media_home, self.media_home)

        if self.get_val("allowExtVal") or self.get_val("allowFavVal"):
            util.make_directory(self.media_home)
            util.make_directory(self.favorites_home)
            util.make_directory(self.external_home)
            epath = os.path.join(self.external_home, ".nomedia")
            open(epath, "a").close()
        self.markListenedBeforeEndThreshold = self.get_val("markListenedEndThreshold")

        pyotherside.send("objectLoaded")

    def get_val(self, valname):
        """
        Return property value by valname
        """

        try:
            return getattr(self.object, valname)
        except:
            return None

    def reload(self):
        logger.info("reloading constants")
        if not db.is_stopped():
            db.stop()
        if db.is_connection_usable():
            db.close()
        self.__initialize(self.progname)

    def close(self):
        logger.info("stopping db")
        db.stop()
        db.close()


class BaseModel(Model):
    class Meta:
        database = db
