"""
The inbox queue. This also uses a Factory for instancing
"""
import logging
import sys
from typing import List, Iterator

from peewee import ModelSelect
from podcast import POST_ID_TYPE
from podcast.AbstractPodpostListEntry import AbstractPodpostListEntry
from podcast.podcast import Podcast
from podcast.podpost import Podpost

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.archive import ArchiveFactory
from podcast.queue import QueueFactory

inboxname = "the_inbox"

logger = logging.getLogger(__name__)


class InboxEntry(AbstractPodpostListEntry):
    @classmethod
    def select_ordered(cls, *fields):
        return cls.select(*fields).order_by(cls.insert_date.desc())


class Inbox:
    """
    The Inbox. It has a list of podposts
    """

    def save(self):
        """
        todo: remove method
        """
        raise NotImplementedError()

    def insert(self, podpost: POST_ID_TYPE):
        """
        Insert an podost
        """

        InboxEntry.of_podpost(podpost).save(force_insert=True)

    def __select_inbox_entries(self, *args):
        return InboxEntry.select(*args).join(Podpost).order_by(Podpost.published.desc())

    def get_podposts(self) -> List[POST_ID_TYPE]:
        """
        get the list of podposts
        """

        return list(e.podpost_id for e in self.__select_inbox_entries())

    def get_podposts_objects(self, podurl=None) -> Iterator[Podpost]:
        query: ModelSelect = self.__select_inbox_entries(Podpost)
        if podurl:
            query = query.join(Podcast).where(Podpost.podcast.url == podurl)
        for e in query:
            yield e.podpost

    def remove(self, podpost: POST_ID_TYPE):
        """
        Remove a podpost from archive
        """

        InboxEntry.delete_by_id(podpost)

    def move_queue_top(self, podpost: POST_ID_TYPE):
        """
        Move this podpost to queue_top (play)
        """

        QueueFactory().get_queue().insert_top(podpost)
        self.remove(podpost)

    def move_queue_next(self, podpost: POST_ID_TYPE):
        """
        Move podpost to next queue position
        """

        QueueFactory().get_queue().insert_next(podpost)
        self.remove(podpost)

    def move_queue_bottom(self, podpost: POST_ID_TYPE):
        """
        Move Podpost to the bottom of Queue
        """

        QueueFactory().get_queue().insert_bottom(podpost)
        self.remove(podpost)

    def move_archive(self, podpost: POST_ID_TYPE):
        """
        Move Podpost to Archive
        """
        ArchiveFactory().get_archive().insert(podpost)
        self.remove(podpost)

    def move_all_archive(self):
        """
        Move all posts to Archive
        """

        archive = ArchiveFactory().get_archive()

        archive.bulk_insert(self.get_podposts())
        InboxEntry.delete().execute()


class InboxFactory(metaclass=Singleton):
    """
    Factory which crates an Inbox if it does not exist or gets the pickle
    otherwise it returns the single element
    """

    def get_inbox(self) -> Inbox:
        """
        Get the Inbox
        """

        return Inbox()
