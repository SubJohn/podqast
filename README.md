# PodQast
A queue based podcast player for SailfishOS

# Testing
Tests can be run with pytest from the test directory.
The manual release testplan is [here](./Manual_Tests.md)

## Coding
Thomas Renard [podqast@g3la.de](mailto:podqast@g3la.de)
Thilo Kogge {podqast@kogge.net}(mailto:podqast@kogge.net)

### Translations

- Åke Engelbrektson [@eson](https://gitlab.com/eson) (sv)
- Carmen F. B. [@carmenfdezb](https://gitlab.com/carmenfdezb) (es)

Please do translations in https://translate.g3la.de

## License
Licensed under GNU GPLv3

## Credits

- [BluesLee](https://talk.maemo.org/member.php?u=3116) (testing)
- [Daniel Noll](http://www.einbilder.de) (logo design)

This project uses
- [feedparser](https://github.com/kurtmckee/feedparser) by Kurt McKee and Mark Pilgrim
- [mygpoclient](http://gpodder.org/mygpoclient/) by Thomas Perl and others
- [html2text](https://github.com/Alir3z4/html2text) by Aaron Schwartz and others
- [mutagen](https://github.com/quodlibet/mutagen) by Christoph Reiter and Joe Wreschnig
- [peewee](https://github.com/coleifer/peewee)
