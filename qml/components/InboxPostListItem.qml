import QtQuick 2.0
import Sailfish.Silica 1.0

PostListItem {

    menu: EpisodeContextMenu {
        id: contextMenu
        favoriteEnabled: false
        archiveEnabled: true
        archivePressedHandler: function (model) {
            inboxhandler.moveArchive(model.id)
        }
    }
}
