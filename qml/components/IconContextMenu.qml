import QtQuick 2.0
import Sailfish.Silica 1.0

ContextMenu {
    default property alias children: container.children
    property alias spacing: container.spacing

    Item {
        height: Theme.itemSizeMedium
        width: parent.width
        Row {
            id: container

            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
                margins: Theme.paddingLarge
            }

            spacing: Theme.paddingLarge
        }
    }
}
