import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

PullDownMenu {
    id: menu
    property bool refreshing: false
    busy: refreshing
    MenuItem {
        id: refreshPodcasts
        text: qsTr("refresh podcasts")
        enabled: !refreshing
        onClicked: {
            menu.refreshing = true
            if (wifiConnected || doMobileDownConf.value) {
                feedparserhandler.refreshPodcasts()
            }
        }

        Connections {
            target: feedparserhandler
            onRefreshPost: {
                menu.refreshing = true
                mainmenulab.text = posttitle ? qsTr("refreshing: ") + posttitle : ""
            }
            onRefreshFinished: {
                menu.refreshing = false
                mainmenulab.text = ""
            }
            onRefreshProgress: {
                refreshPodcastsProgressBar.value = progress * 1000
            }
        }
    }
    MenuLabel {
        id: mainmenulab
        visible: !refreshPodcasts.enabled
    }
    ProgressBar {
        id: refreshPodcastsProgressBar
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        visible: !refreshPodcasts.enabled
        minimumValue: 0
        maximumValue: 1000
    }
}
