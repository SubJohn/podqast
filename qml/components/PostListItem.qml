import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0
import "timeutil.js" as Timeutil

ListItem {
    property string title: model.title
    property string detail: model.detail
    property var length: model.length
    property var fdate: model.fdate
    property double duration: model.duration
    property var postId: model.id
    property string link: model.link
    property string logo_url: model.podcast_logo
    property double position: model.position
    property string description: model.description
    property bool favorite: model.favorite
    property bool listened: model.listened

    property bool showDescription: true
    property bool disableListenedHint: false
    property bool isPlaying: false

    width: ListView.view.width
    contentHeight: Theme.itemSizeExtraLarge + Theme.fontSizeExtraSmall

    onClicked: pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), {
                                  "title": title,
                                  "detail": detail,
                                  "length": length,
                                  "date": date,
                                  "duration": duration,
                                  "href": link,
                                  "episode_logo": model.episode_logo
                              })
    clip: true
    property bool isfavorite: favorite

    Rectangle {
        id: image_replacement
        anchors.fill: episodeImage
        visible: logo_url === ""
        color: Theme.rgba(Theme.highlightColor, 0.5)

        clip: true

        Label {
            anchors.centerIn: parent
            font.pixelSize: parent.height * 0.8
            text: (title) ? title[0] : "-"
            color: Theme.highlightColor
        }
    }

    EpisodeImage {
        id: episodeImage
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        showIsExternalAudio: is_external_audio
        showIsPlaying: postId === playerHandler.firstid
        showIsListened: listened
        percentage: model.dlperc
    }

    Item {
        anchors.left: episodeImage.right
        anchors.right: parent.right
        anchors.margins: Theme.paddingMedium
        height: parent.height
        clip: true

        Label {
            id: episodeTitle

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top

            text: title
            font.pixelSize: Theme.fontSizeExtraSmall
            font.bold: true
            wrapMode: Text.WordWrap
            maximumLineCount: 3
        }

        Label {
            id: episodeText

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: episodeTitle.bottom
            anchors.bottom: parent.bottom

            font.pixelSize: Theme.fontSizeTiny
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            text: ''
            truncationMode: TruncationMode.Elide
            topPadding: Theme.paddingMedium

            Component.onCompleted: updateDescriptionLabel()

            function updateDescriptionLabel() {
                if (isPlaying)
                    return qsTr("playing")
                var timestr = ""
                var labelstr = ""
                if (listened && !disableListenedHint) {
                    timestr += qsTr("listened")
                } else {
                    if (position > 0) {
                        timestr += Timeutil.format_duration_ms(
                                    duration - position) + ' ' + qsTr(
                                    "remaining")
                    } else {
                        timestr += Timeutil.format_duration_ms(duration)
                    }
                }
                labelstr = "<b>" + timestr + "</b>"

                if (showDescription) {
                    labelstr += " · " + description
                }
                text = labelstr
            }
        }
    }
}
