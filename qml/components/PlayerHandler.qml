import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    id: playerHandler

    property real position: (lastPlayedId != firstid
                             || mediaplayer.position <= 0) ? positionFromDb : mediaplayer.position
    property real duration: (lastPlayedId != firstid
                             || mediaplayer.duration <= 0) ? durationFromDb : mediaplayer.duration
    property real positionSeconds: position / 1000
    property real durationSeconds: duration / 1000
    property real durationFromDb: 0
    property real positionFromDb: 0

    property bool isPlaying: false
    property string podcast_logo: "../../images/podcast.png"
    property string podcast_logo_or_fallback: podcast_logo ? podcast_logo : "../../images/podcast.png"
    property string episode_logo: ""
    property string episode_logo_or_fallback: episode_logo ? episode_logo : podcast_logo
    property string playtext: ""
    property string firstid: ""
    property string lastPlayedId: ""
    property string firsttitle: ""
    property string firstPodcastTitle: ""
    property var chapters
    property int aktchapter
    property double playpos: 0
    property double playrate: globalPlayrateConf.value
    property double seekPos
    property bool doStartAfterSeek: false
    property bool streaming: mediaplayer.source && !/^file:/.test(
                                 mediaplayer.source)

    signal playing(string audio_url, int position, bool only_start_if_playing)
    signal pausing
    signal stopping
    signal downloading(int percent)
    signal audioNotExist

    onDurationChanged: console.log(firstid + ": duration=" + duration)

    Component.onCompleted: {
        setHandler('playing', playing)
        setHandler('pausing', pausing)
        setHandler('downloading', downloading)
        setHandler('stopping', stopping)
        setHandler('audioNotExist', audioNotExist)

        addImportPath(Qt.resolvedUrl('components'))
        importModule('QueueHandler', function () {
            console.log('QueueHandler is now imported')
        })
    }

    onError: {
        console.log('python error: ' + traceback)
    }

    onPlaying: {
        console.info("Playing audio_url: " + audio_url + " @ rate " + playrate
                     + "@ position " + position)
        positionFromDb = position
        if (audio_url != "") {
            if (mediaplayer.source != audio_url) {
                mediaplayer.source = audio_url
                mediaplayer.seek(positionFromDb)
            }
            if (!only_start_if_playing || mediaplayer.isPlaying) {
                lastPlayedId = firstid
                mediaplayer.play()
            }
            seekPos = positionFromDb
        } else if (!only_start_if_playing || mediaplayer.isPlaying) {
            mediaplayer.play()
            isPlaying = true
        }

        console.debug("Duration: ", mediaplayer.duration + " Position: ",
                      mediaplayer.position)
        externalhandler.getAudioData(audio_url)
    }

    onPausing: {
        mediaplayer.pause()
        var pauseRewindTime = 1000
        var positionToSave = mediaplayer.position
        if (mediaplayer.position > pauseRewindTime)
            positionToSave = mediaplayer.position - pauseRewindTime
        isPlaying = false
        queuehandler.updatePlayingPosition(lastPlayedId, positionToSave)
    }

    onStopping: {
        mediaplayer.pause()
        isPlaying = false
        queuehandler.updatePlayingPosition(lastPlayedId, mediaplayer.position)
    }

    onDownloading: {

    }

    // sets data relevant for the player display
    // not handling mediaplayer url etc
    function setEpisode(data, chapterlist) {
        firstid = data.id
        firsttitle = data.title
        firstPodcastTitle = data.podcastTitle
        chapters = chapterlist
        podcast_logo = data.podcast_logo
        if (data.episode_logo)
            episode_logo = data.episode_logo
        else
            episode_logo = ""
        playtext = data.title
        durationFromDb = data.duration
        positionFromDb = data.position

        console.log("setting episode: " + data.title + " url:" + data.audio_url
                    + " duration:" + data.duration + " position: " + positionFromDb)
    }

    function getaktchapter() {
        if (chapters.length === 0) {
            return
        }
        var thepos = mediaplayer.position
        for (var i = 0; i < chapters.length; i++) {
            var startpos = chapters[i].start_millis
            if (startpos > thepos) {
                aktchapter = i - 1
                return
            }
        }
        aktchapter = chapters.length - 1
    }

    function nextchapter() {
        console.log("started nextchapter()")
        if (aktchapter !== chapters.length - 1) {
            aktchapter += 1
            playpos = chapters[aktchapter].start_millis
            seek(playpos)
            mediaplayer.seek(playpos)
        } else {
            //go to end of track if there is no next chapter
            playpos = mediaplayer.duration
            seek(playpos)
            mediaplayer.seek(playpos)
        }
    }

    function nextselectedchapter() {
        //skip to the next selected chapter
        for (var i = aktchapter; i < chapters.length; i++) {
            console.log("chapter " + i + "from" + chapters.length
                        + "is selected: " + chapters[i].selected)
            if (chapters[i].selected === true) {
                playpos = chapters[i].start_millis
                seek(playpos)
                mediaplayer.seek(playpos)
                return
            }
        }
        //go to end of track if there is no next chapter
        console.log("no more selected chapters. skipping to end of track.")
        playpos = mediaplayer.duration
        seek(playpos)
        mediaplayer.seek(playpos)
    }

    function play() {
        isPlaying = true
        call("QueueHandler.instance.queue_play", function () {})
    }

    function pause() {
        isPlaying = false
        call("QueueHandler.instance.queue_pause", [mediaplayer.position],
             function () {})
    }

    function playpause() {
        console.info("Toggle play: " + isPlaying)
        if (!isPlaying) {
            play()
        } else {
            pause()
        }
    }

    function stop() {
        console.log("mediaplayer.position" + mediaplayer.position)
        call("QueueHandler.instance.queue_stop", [mediaplayer.position],
             function () {})
    }

    function seek(position) {
        call("QueueHandler.instance.queue_seek", [position], function () {})
        mediaplayer.seek(position)
    }

    function setDuration() {
        if (mediaplayer.duration > 0) {
            call("QueueHandler.instance.set_duration", [mediaplayer.duration],
                 function () {
                     durationFromDb = mediaplayer.duration
                 })
        }
    }

    function fast_backward() {
        var posi = mediaplayer.position
        posi = posi - 15 * 1000
        if (posi < 0) {
            posi = 0
        }
        seek(posi)
    }

    function fast_forward() {
        var posi = mediaplayer.position
        posi = posi + 30 * 1000
        if (mediaplayer.duration > 0 && posi > mediaplayer.duration) {
            posi = mediaplayer.duration
        }
        seek(posi)
    }

    function interval_task() {
        if (lastPlayedId)
            queuehandler.updatePlayingPosition(lastPlayedId,
                                               mediaplayer.position)
    }
}
