# -*- coding: utf-8 -*-
import asyncio
from typing import Iterator

import pyotherside
from podcast.podpost import Podpost
from podcast.util import chunks
from podcast.archive import ArchiveFactory
import logging

logger = logging.getLogger(__name__)


async def get_archive_posts(podurl=None):
    """
    Return a list of all archive posts
    """

    posts: Iterator[Podpost] = ArchiveFactory().get_archive().get_podpost_objects(url_filter=podurl,
                                                                                  sort_by_insertdate=True, limit=1000)
    for offset, max, chunk in chunks(posts, 32):
        pyotherside.send("historyData", offset, [post.get_data() for post in chunk])


class ArchiveHandler:

    def getarchiveposts(self, podurl=None):
        asyncio.run(get_archive_posts(podurl))


archivehandler = ArchiveHandler()
