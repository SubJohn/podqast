import QtQuick 2.0
import Sailfish.Silica 1.0
import "../../lib/hints"
import "../"

SlideShowHint {

    slides: [{
            "text": qsTr("Hi there, I gonna give you a quick tour to show you basic and new features."),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 3
        }, {
            "text": qsTr("Quick touch on an episode to see its description, long press it to add it to queue or play it"),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 5
        }, {
            "text": qsTr("On an episode image, this icon shows that it is downloaded..."),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 3,
            "centeredComponent": "../../components/hints/HintEpisodeImage.qml"
        }, {
            "text": qsTr("...currently playing..."),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 2,
            "centeredComponent": "../../components/hints/HintEpisodeImage.qml"
        }, {
            "text": qsTr("...already listened."),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 2,
            "centeredComponent": "../../components/hints/HintEpisodeImage.qml"
        }, {
            "text": qsTr("In the dock below, this icon means the file currently played is streaming from the internet right now..."),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 4,
            "centeredComponent": "../../components/hints/HintDockIcons.qml"
        }, {
            "text": qsTr("...the sleeptimer is active"),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 2,
            "centeredComponent": "../../components/hints/HintDockIcons.qml"
        }, {
            "text": qsTr("...a playrate different from 1 is set."),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 2,
            "centeredComponent": "../../components/hints/HintDockIcons.qml"
        }, {
            "text": qsTr("You can see whats going on by looking into the log down here"),
            "direction": TouchInteraction.Up,
            "interactionMode": TouchInteraction.EdgeSwipe,
            "loops": 3
        }, {
            "text": qsTr("Podqast supports pagination of feeds. If you are missing old episodes of feeds, you can find a force refresh button in the settings."),
            "direction": TouchInteraction.Up,
            "interactionMode": TouchInteraction.EdgeSwipe,
            "loops": 6
        }]
}
