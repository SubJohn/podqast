import QtQuick 2.0
import Sailfish.Silica 1.0
import "../../lib/hints"
import "../"

SlideShowHint {
    slides: [{
            "text": qsTr("Select if the next item of the playlist should be played automatically"),
            "direction": TouchInteraction.Down,
            "interactionMode": TouchInteraction.EdgeSwipe,
            "loops": 3
        }, {
            "text": qsTr("Click the image to adjust the playrate"),
            "direction": 0,
            "interactionMode": 0,
            "interactionHidden": true,
            "loops": 3
        }]
}
